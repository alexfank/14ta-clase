# Primer Print Project de Acamica

API de festión de pedidos del restairant Delilah Restó.

## Instalación

Utilizamos el administrador de paquetes de Node.

```bash
npm install
````

## Usamos

```javascript
express -v 4.17.1
nodemon -v 2.0.12
swagger-jsdoc -v 6.1.0
swagger-ui-express -v 4.1.6
```
## Alumno / Desarrollador
Alex Fank
