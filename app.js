const express = require('express');
const { parse } = require('ipaddr.js');
const app = express();

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

//Configurar los Middleware
//Este método se llama como middleware
app.use(express.json());
//es un método incorporado en express para reconocer el objeto de solicitud entrante como objeto JSON.
//Este método se llama como middleware
app.use(express.urlencoded({ extended: true }));

//CONFIGURAR SWAGGER
const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: '1er Sprint Acmamica',
        version: '1.0.0'
      }
    },
    apis: ['./app.js'],
  };

//CONFIGURAR EL SwaggerDocs
  const swaggerDocs = swaggerJsDoc(swaggerOptions);

//CONFIGURAR SERVER PARA EL USO DE SWAGGER, O CONFIGURAR EL ENDPOINT

app.use('/api-docs',
   swaggerUI.serve,
   swaggerUI.setup(swaggerDocs));

//ARRAY USUARIOS
let users = [{
    user: "admin",
    pass: "admin",
    nameLast: "Alex Fank",
    phone: "",
    address: "Marcos Zar 1338",
    email: "",
    admin: true,
    logged: false
}];

let loggedUser = "";
let userID;

//ARRAY PRODUCTOS
let products = [{
    productID: 1,
    name: "Bagel de Salmón",
    price: 425
},{
    productID: 2,
    name: "Hamburguesa Clásica",
    price: 350
},{
    productID: 3,
    name: "Sandwich Veggie",
    price: 310
},{
    productID: 4,
    name: "Ensalada Veggie",
    price: 340
},{
    productID: 5,
    name: "Focaccia",
    price: 300
},{
    productID: 6,
    name: "Sandwich Focaccia",
    price: 440
}];

const new_product = {
    id: "",
    name: "",
    price: "",
};

//ARRAY PEDIDOS
let orders = [];

const new_order = {
    userID: "",
    orderID: "",
    items: [],
    paymentMethod: "",
    deliveryAddress: "",
    pedidoNuevo: true,
    confirmado: false,
    enPreparacion: false,
    enviado: false,
    entregado: false,
    total: ""
};

//ARRAY MEDIOS DE PAGO
let paymentMethod = [{
    payID: 1,
    name: "Efectivo"
},{
    payID: 2,
    name: "VISA Crédito"
},{
    payID: 3,
    name: "VISA Débito"
},{
    payID: 4,
    name: "Pay Pal"
}];
    
const new_pay = {
    payID: "",
    name: "",
    percentage: "",
};

//MIDDLEWARES

const userValidation = (req,res,next) => {
    let userVal = users.find(user => req.body.user == user.user)
    if(!userVal){
        res.status(400).send('El usuario ingresado no existe');
    };
    next();
};

const passValidation = (req,res,next) => {
    let passVal = users.find(pass => req.body.pass == pass.pass);
    if(!passVal){
        res.status(400).send('La contraseña es incorrecta');
    };
    next();
};

const duplicateUser = (req,res,next) => {
    let userDup = users.find(user => req.body.user == user.user);
        if(userDup){
            return res.status(400).send('El usuario ya existe')
        };
    next();
};

const duplicateEmail = (req,res,next) => {
    let emailDup = users.find(email => req.body.email == email.email);
        if(emailDup){
            return res.status(400).send('El email ya existe')
        };
    next();
};

const isAdmin = (req,res,next) => {
    users.forEach(user => {
        if(user.admin !== true){
            res.status(403).send('El usuario no es administrador');
        }
    });
    next();
};

const isLogged = (req,res,next) => {
    let found = users.find(user => user.logged == true);
    if(found){
        next();
    }else{
        return res.status(401).send('Usuario no logueado');
    }
};

const productValidation = (req,res,next) => {
    let itemVal = req.body.itemID;
    let validate = products.filter(product => itemVal == product.productID)
    if(validate[0].productID != parseInt(itemVal)){
        res.status(400).send('El ID del producto no existe');
    }
    next();
};

const orderValidation = (req,res,next) => {
    let orderVal = orders.find(order => parseInt(req.body.orderID) == order.orderID)
        if(!orderVal){
            return res.status(400).send('El ID de la orden no existe');
        }
    next();
};

const paymentValidation = (req,res,next) => {
    let payVal = paymentMethod.find(pay => parseInt(req.body.id) == pay.payID)
        if(!payVal){
           return res.status(400).send('El ID de la forma de pago no existe');
        }
    next();
};

const orderClosed = (req,res,next) => {
    orderCl = orders.find(order => parseInt(req.body.orderID) == order.orderID)
        if(orderCl.enPreparacion !== false || orderCl.enviado !== false || orderCl.entregado !== false){
            return res.status(403).send('Ya no puede modificar la orden ya que se encuentra cerrada.');
        }
    next();
};

//ENDPOINTS

//LOGIN

/**
 * @swagger
 * /login:
 *  post:
 *    description: Login de usuario
 *    parameters:
 *    - name: user
 *      description: Usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: pass
 *      description: Contraseña
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */
app.post('/login',userValidation,passValidation,(req,res)=>{
    let userLogin = req.body.user;
    let passLogin = req.body.pass;
    users.forEach(user => {
        if(userLogin == user.user && passLogin == user.pass){
                userID = users.indexOf(user);
                user.logged = true;
                loggedUser = user.user;
                return res.status(200).json('Usuario logueado correctamente')
        };
    });
});

//ALTA USUARIOS

/**
 * @swagger
 * /login/new:
 *  post:
 *    description: Creación de usuario
 *    parameters:
 *    - name: user
 *      description: Usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: pass
 *      description: Contraseña
 *      in: formData
 *      required: true
 *      type: string
 *    - name: nameLast
 *      description: Nombre y Apellido
 *      in: formData
 *      required: true
 *      type: string
 *    - name: phone
 *      description: Telefono
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: address
 *      description: Dirección
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.post('/login/new',duplicateUser,duplicateEmail,(req,res)=>{
    const {user, pass, nameLast, phone, address, email} = req.body;
    const new_user = {
        user: user,
        pass: pass,
        nameLast: nameLast,
        phone: phone,
        address: address,
        email: email,
        admin: false,
        logged: false
    };
    users.push(new_user);
    console.log('Usuario creado exitosamente');
    res.status(201).json({new_user});
});

// GET USUARIOS

/**
 * @swagger
 * /users:
 *  get:
 *    description: Enlista usuarios
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.get('/users',(req,res)=>{
    res.status(200).send(users)
})

// PRODUCTOS

/**
 * @swagger
 * /products:
 *  get:
 *    description: Enlista productos
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.get('/products',isLogged,(req,res)=>{
    res.status(200).send(products)
})

/**
 * @swagger
 * /products/new:
 *  post:
 *    description: Creación de producto
 *    parameters:
 *    - name: name
 *      description: Nombre del producto
 *      in: formData
 *      required: true
 *      type: string
 *    - name: price
 *      description: Precio del producto
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.post('/products/new/',isLogged,isAdmin,(req,res)=>{
    const {name, price} = req.body;
    let newID = products[0].productID;
    products.forEach(product => {
        if(product.productID>newID){
            newID = product.productID
        }
    });
    const new_product = {
        productID: parseInt(newID)+1,
        name: name,
        price: parseInt(price)
    };
    products.push(new_product);
    console.log('Producto creado exitosamente');
    res.status(201).send(new_product);
    });

/**
 * @swagger
 * /products/mod:
 *  put:
 *    description: Modificación de producto
 *    parameters:
 *    - name: id
 *      description: ID del producto
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: name
 *      description: Nombre del producto
 *      in: formData
 *      required: true
 *      type: string
 *    - name: price
 *      description: Precio del producto
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.put('/products/mod/',isLogged,isAdmin,(req,res)=>{
    const {id, name, price} = req.body;
    let posProd = id;
    products.forEach(product => {
        if(posProd == product.productID){
            posProd = products.indexOf(product);
        }
    });
    const new_product = {
        productID: parseInt(id),
        name: name,
        price: price
    };
    
    products[posProd] = {
        productID: id,
        name: name,
        price: price
    };
    console.log('Producto modificado correctamente');
    res.status(200).send(new_product);
    });

/**
 * @swagger
 * /products/del:
 *  delete:
 *    description: Eliminación de producto
 *    parameters:
 *    - name: id
 *      description: ID del producto
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.delete('/products/del/',isLogged,isAdmin,(req,res)=>{
    let posProd = parseInt(req.body.id);
    products.forEach(product => {
        if(posProd == product.productID){
            posProd = products.indexOf(product);
        }
    });

    products.splice(posProd,1)
    res.status(200).send('Producto eliminado correctamente');
    });

//PEDIDOS

/**
 * @swagger
 * /orders/all:
 *  get:
 *    description: Enlista todas las ordenes realizadas
 *    responses:
 *      200:
 *        description: Success
 * 
 */

 app.get('/orders/all',isLogged,isAdmin,(req,res)=>{
    res.status(200).send(orders)
});

//ORDENES

/**
 * @swagger
 * /orders:
 *  get:
 *    description: Enlista las ordenes realizadas por el usuario
 *    responses:
 *      200:
 *        description: Success
 * 
 */

 app.get('/orders',isLogged,(req,res)=>{
    let orders_temp = [];
    orders.forEach((order,i) => {
        usuario = order.userID;
        if(usuario === userID){
            orders_temp.push(order)
        }
    });
    res.status(200).send(orders_temp);
});

/**
 * @swagger
 * /orders/new:
 *  post:
 *    description: Creación de una orden
 *    parameters:
 *    - name: itemID
 *      description: ID del producto a ordenar.
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: qty
 *      description: Cantidad del producto a ordenar.
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: deliveryAddress
 *      description: Dirección de envío. En caso de no completar se enviará a la dirección del registro del usuario.
 *      in: formData
 *      required: false
 *      type: string
 *    - name: payMeth
 *      description: Selecciona método de pago
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.post('/orders/new',isLogged,productValidation,(req,res)=>{
    const {itemID, qty, deliveryAddress, payMeth} = req.body;
    let orderID = 0;
    orders.forEach(order => {
        if(order.orderID > orderID){
            orderID = order.orderID;
        }
    });

    let payPos = paymentMethod.find(pay => payMeth == pay.payID)

    let quantity = parseInt(qty);
    let itemProduct = parseInt(itemID);
    let itemPrice = 0;
    let delAddress = deliveryAddress;
    if(!delAddress){
        delAddress = users[userID].address;
    };
    products.forEach(product => {
        if(itemProduct == product.productID){
            itemProduct = product.name;
            itemPrice = parseInt(product.price);
        }
    });


    let new_order = {
        userID: userID,
        orderID: orderID+1,
        items: [{itemProduct, quantity, itemPrice}],
        paymentMethod: paymentMethod[paymentMethod.indexOf(payPos)],
        deliveryAddress: delAddress,
        pedidoNuevo: true,
        confirmado: false,
        enPreparacion: false,
        enviado: false,
        entregado: false,
        total: (itemPrice * parseInt(qty))
    };

    orders.push(new_order)
    res.status(201).json(new_order)
});

/**
 * @swagger
 * /orders/add:
 *  put:
 *    description: Adhición de productos a la orden
 *    parameters:
 *    - name: orderID
 *      description: ID de la orden
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: itemID
 *      description: ID del producto a agregar.
 *      in: formData
 *      required: true
 *      type: integer   
 *    - name: qty
 *      description: Cantidad del producto a agregar.
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

 app.put('/orders/add/',isLogged,productValidation,orderValidation,orderClosed,(req,res)=>{
    const {orderID, itemID, qty} = req.body;
    
    let itemProduct = parseInt(itemID);
    let posOrder;
    let itemPrice = 0;
    let quantity = parseInt(qty);

    products.forEach(product => {
        if(itemProduct == product.productID){
            itemProduct = product.name;
            itemPrice = product.price;
        }
    });

    orders.forEach(order => {
        if(orderID == order.orderID){
            posOrder = orders.indexOf(order);
        }
    });

    orders[posOrder].items.push({itemProduct, quantity, itemPrice});
    orders[posOrder].total = (orders[posOrder].total + (itemPrice * parseInt(qty)));
    res.status(200).send('Se ha agregado el item ' + itemProduct + ' a su orden.');
    });

/**
 * @swagger
 * /orders/status:
 *  put:
 *    description: Modificación de estado de orden
 *    parameters:
 *    - name: orderID
 *      description: ID de la orden
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: status1
 *      description: Estado pedidoNuevo (Indicar true o false)
 *      in: formData
 *      required: true
 *      type: boolean
 *    - name: status2
 *      description: Estado confirmado (Indicar true o false)
 *      in: formData
 *      required: true
 *      type: boolean
 *    - name: status3
 *      description: Estado enPreparacion (Indicar true o false)
 *      in: formData
 *      required: true
 *      type: boolean
 *    - name: status4
 *      description: Estado enviado (Indicar true o false)
 *      in: formData
 *      required: true
 *      type: boolean
 *    - name: status5
 *      description: Estado entregado (Indicar true o false)
 *      in: formData
 *      required: true
 *      type: boolean
 *    responses:
 *      200:
 *        description: Success
 * 
 */

 app.put('/orders/status/',isLogged,isAdmin,orderValidation,(req,res)=>{
    const {orderID, status1, status2, status3, status4, status5} = req.body;
    let orderMod = orders.find(order => parseInt(orderID) == order.orderID);

    orderMod.pedidoNuevo = status1;
    orderMod.confirmado = status2;
    orderMod.enPreparacion = status3;
    orderMod.enviado = status4;
    orderMod.entregado = status5;
    res.status(200).send('Orden modificada correctamente');
    });

//MEDIOS DE PAGO

/**
 * @swagger
 * /payment:
 *  get:
 *    description: Enlista todos los métodos de pago.
 *    responses:
 *      200:
 *        description: Success
 * 
 */

 app.get('/payment',isLogged,isAdmin,(req,res)=>{
    res.status(200).send(paymentMethod)
});

/**
 * @swagger
 * /payment/new:
 *  post:
 *    description: Creación de método de pago
 *    parameters:
 *    - name: name
 *      description: Nombre del producto
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

 app.post('/payment/new/',isLogged,isAdmin,(req,res)=>{
    const {name} = req.body;

    let payID = 0;
    paymentMethod.forEach(pay => {
        if(pay.payID > payID){
            payID = pay.payID;
        }
    });

    const new_payment = {
        payID: payID+1,
        name: name
    };
    paymentMethod.push(new_payment);
    console.log('Método de pago agregado exitosamente');
    res.status(201).send(new_payment);
    });

/**
 * @swagger
 * /payment/mod:
 *  put:
 *    description: Modificación de método de pago
 *    parameters:
 *    - name: id
 *      description: ID del método de pago
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: name
 *      description: Nombre del método de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.put('/payment/mod/',isLogged,isAdmin,paymentValidation,(req,res)=>{
    const {id, name} = req.body;
    let posPay = id;
    paymentMethod.forEach(payment => {
        if(posPay == payment.payID){
            posPay = paymentMethod.indexOf(payment);
        }
    });
    
    paymentMethod[posPay] = {
        payID: parseInt(id),
        name: name
    };
    res.status(200).send('Método de pago modificado correctamente');
    });

/**
 * @swagger
 * /payment/del:
 *  delete:
 *    description: Eliminación de método de pago
 *    parameters:
 *    - name: id
 *      description: ID del método de pago
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.delete('/payment/del/',isLogged,isAdmin,paymentValidation,(req,res)=>{
    let posPay = req.body.id;
    paymentMethod.forEach(payment => {
        if(posPay == payment.payID){
            posPay = paymentMethod.indexOf(payment);
        }
    });

    paymentMethod.splice(posPay,1)
    res.status(200).send('Método de pago eliminado correctamente');
    });

//PEDIDOS

/**
 * @swagger
 * /orders/all:
 *  get:
 *    description: Enlista todas las ordenes realizadas
 *    responses:
 *      200:
 *        description: Success
 * 
 */

 app.get('/orders/all',isLogged,isAdmin,(req,res)=>{
    res.status(200).send(orders)
});

app.listen(3000, () => console.log('Corriendo en el puerto 3000'));